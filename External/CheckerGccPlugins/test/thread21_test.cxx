// Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration.
// thread21_test: testing check_thread_safe_call,
//                 for virtual function calls

#pragma ATLAS check_thread_safety


class C
{
public:
  virtual void foo [[ATLAS::not_thread_safe]] () const;
  virtual void bar [[ATLAS::not_thread_safe]] () const = 0;
};

void class_test (const C& cr, const C* cp)
{
  cr.foo();
  cr.bar();
  cp->foo();
  cp->bar();
}
