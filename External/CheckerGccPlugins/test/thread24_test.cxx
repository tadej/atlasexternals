// Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration.
// thread24_test: testing check_nonconst_pointer_member_passing

#pragma ATLAS check_thread_safety

namespace std {
template <class T>
class unique_ptr
{
public:
  T& operator* [[ATLAS::not_const_thread_safe]] () const { return *m_p; }
  T* get [[ATLAS::not_const_thread_safe]] () const { return m_p; }
  T* release [[ATLAS::not_const_thread_safe]] () { return m_p; }
private:
  T* m_p;
};
}

class IService
{
public:
  virtual ~IService();
};
class Service : public IService {};
class AthService : public Service {};
class Helper : public AthService {};

struct MyTool {
  void doIt1() const;
  void doIt2();
  int* m_p;
  std::unique_ptr<int> m_up;
  Helper* m_svc;
};


void foo(int& x);
void foo(int* x);
void fooc(const int& x);
void fooc(const int* x);
void foo(Helper*);


void MyTool::doIt1() const
{
  foo (m_p);  // warning
  foo (*m_p);  // warning
  foo (m_up.get());  // warning
  foo (*m_up);  // warning

  fooc (m_p);  // no warning
  fooc (*m_p);  // no warning
  fooc (m_up.get());  // no warning
  fooc (*m_up);  // no warning

  int* p1 = m_p;
  foo (p1); // warning
  foo (*p1); // warning
  fooc (p1); // no warning
  fooc (*p1); // no warning

  int* p2 = m_up.get();
  foo (p2); // warning
  foo (*p2); // warning
  fooc (p2); // no warning
  fooc (*p2); // no warning

  int* p3 [[ATLAS::thread_safe]] = m_p;
  foo (p3); // no warning
  foo (*p3); // no warning

  int* p4 [[ATLAS::thread_safe]] = m_up.get();
  foo (p4); // no warning
  foo (*p4); // no warning

  foo (m_svc); // no warning
}


void MyTool::doIt2() // no warnings
{
  foo (m_p);
  foo (*m_p);
  foo (m_up.get());
  foo (*m_up);

  int* p1 = m_p;
  foo (p1);

  int* p2 = m_up.get();
  foo (p2);
}
